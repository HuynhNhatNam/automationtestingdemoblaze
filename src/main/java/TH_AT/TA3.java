package TH_AT;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class TA3 {
    public WebDriver edriver;
    public WebDriverWait ewait;


    @FindBy(how = How.XPATH, using = "//a[@id='signin2']")
    public WebElement Signin1; //Sign up

    @FindBy(how = How.XPATH, using = "//input[@id='sign-username']")
    public WebElement SignUserName; //Sign in username

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Sign up')]")
    public WebElement SignUserBtnClick; //Click btn Sign in

    @FindBy(how = How.XPATH, using = "//body/div[@id='signInModal']//div[3]/button[1]")
    public WebElement SignCloserBtnClick; //Click btn Close
    @FindBy(how = How.XPATH, using = "//input[@id='sign-password']")
    public WebElement SignUserPass;
    @FindBy(how = How.XPATH, using = "//*[@id='login2']")
    public WebElement Login1;

    @FindBy(how = How.XPATH, using = "//*[@id='loginusername']")
    public  WebElement userName;

    @FindBy(how = How.XPATH, using = "//*[@id='loginpassword']")
    public  WebElement userPass;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Log in')]")
    public  WebElement Loguser;

    public TA3() {
        edriver = new ChromeDriver();
        PageFactory.initElements(edriver, this);
        ewait = new WebDriverWait(edriver, Duration.ofSeconds(5));
    }
    @Before
    public void setUp() {
        edriver.get("https://www.demoblaze.com/");
        edriver.manage().window().maximize();
    }
    @Test
    public void test() throws InterruptedException {
        Thread.sleep(2000);

        //Click Sign up
        waitUntilElementVisible(Signin1);
        Signin1.click();

        //Sendkeys Sign Username
        waitUntilElementVisible(SignUserName);
        SignUserName.sendKeys("NhatNam");

        //Sendkeys Sign Password
        waitUntilElementVisible(SignUserPass);
        SignUserPass.sendKeys("123");

        //Click Sign Btn
        waitUntilElementVisible(SignUserBtnClick);
        SignUserBtnClick.click();
        Thread.sleep(2000);
        edriver.switchTo().alert().accept(); //accept alert

        //Click Sign btn Close
        Thread.sleep(2000);
        waitUntilElementVisible(SignCloserBtnClick);
        SignCloserBtnClick.click();

        //Click Login
        waitUntilElementVisible(Login1);
        Login1.click();
        //
        waitUntilElementVisible(userName);
        userName.sendKeys("NhatNam");
        //
        waitUntilElementVisible(userPass);
        userPass.sendKeys("123");
        //
        waitUntilElementVisible(Loguser);
        Loguser.click();
        //

    }
    @After
    public void tearDown() {
        //edriver.close();
    }

    public void waitUntilElementVisible(WebElement element)  {
        int tryTimes = 0;
        while (tryTimes < 2) {
            try {
                ewait.until(ExpectedConditions.visibilityOf(element));
                ewait.until(ExpectedConditions.elementToBeClickable(element));
                break;
            }
            catch (StaleElementReferenceException se) {
                tryTimes ++;
                if (tryTimes == 2)
                    throw se;
            }
        }
    }

    public void waitUntilElementVisible(String path) throws Exception {
        int tryTimes = 0;
        while (tryTimes < 2) {
            try {
                WebElement element = edriver.findElement(By.id(path));
                ewait.until(ExpectedConditions.visibilityOf(element));
                ewait.until(ExpectedConditions.elementToBeClickable(element));
                break;
            }
            catch (StaleElementReferenceException se) {
                tryTimes ++;
                if (tryTimes == 2)
                    throw se;
            }
        }
    }
}
