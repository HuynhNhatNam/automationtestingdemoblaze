package FinalProject_DemoBlaze;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FinalProject extends CommonPageObjectFinal {
    public WebDriver edriver;
    public WebDriverWait ewait;

    DataField_FinalProject datafield;

    public FinalProject() throws Exception {
        super(CommonPageObjectFinal.edriver);
        edriver = CommonPageObjectFinal.edriver;
        ewait = CommonPageObjectFinal.ewait;
        datafield = new DataField_FinalProject("src/main/java/FinalProject_DemoBlaze/Excel_FinalProject.xlsx");
    }
    @Test
    public void test() throws Exception {
        //Begin TestCase 1: Sign Up
        waitUntilElementVisible(Signin1);
        clickElement(Signin1);
        //
        waitUntilElementVisible(SignUserName);
        try {
            Assert.assertTrue(SignUserName.isDisplayed());
            System.out.println("Mở form đăng ký thành công");
        } catch (Exception e) {
            System.out.println("Mở form đăng ký không thành công");
        }
        sendKeyElement(SignUserName, datafield.getData(0, 0));
        //
        waitUntilElementVisible(SignUserPass);
        sendKeyElement(SignUserPass, datafield.getData(0, 1));
        //
        waitUntilElementVisible(SignUserBtnClick);
        clickElement(SignUserBtnClick);
        Thread.sleep(2000);
        ewait.until(ExpectedConditions.alertIsPresent());
        try {
            Alert alert = edriver.switchTo().alert();
            String alertText = alert.getText();
            System.out.println("Thông báo hiển thị: " + alertText);
            Thread.sleep(1000);
            alert.accept();
        } catch (NoAlertPresentException e) {
            System.out.println("Không tìm thấy thông báo");
        }
        // Đóng form đăng ký
        Thread.sleep(2000);
        waitUntilElementVisible(SignCloserBtnClick);
        clickElement(SignCloserBtnClick);
        //End TestCase 1

        //Begin TestCase 2: Login
        //Login
        Thread.sleep(2000);
        waitUntilElementVisible(Login1);
        try {
            Assert.assertTrue(Login1.isDisplayed());
            System.out.println("Mở form đăng nhập thành công");
        } catch (Exception e) {
            System.out.println("Mở form đăng nhập không thành công");
        }
        clickElement(Login1);
        //
        Thread.sleep(2000);
        waitUntilElementVisible(userName);
        try {
            Assert.assertTrue(userName.isDisplayed());
            System.out.println("Điền thông tin username thành công");
        } catch (Exception e) {
            System.out.println("Điền thông tin username không thành công");
        }
        sendKeyElement(userName, datafield.getData(0,0));
        //
        waitUntilElementVisible(userPass);
        try {
            Assert.assertTrue(userPass.isDisplayed());
            System.out.println("Điền thông tin password thành công");
        } catch (Exception e) {
            System.out.println("Điền thông tin password không thành công");
        }
        sendKeyElement(userPass, datafield.getData(0,1));
        //
        String actualResult = edriver.getTitle();
        String expectedResult = "STORE";
        try {
            Assert.assertEquals(actualResult, expectedResult);
            System.out.println("Math Title");
        } catch (Exception e) {
            System.out.println("Not Math Title");
        }
        waitUntilElementVisible(Loguser);
        clickElement(Loguser);
        Thread.sleep(2000);
        waitUntilElementVisible(welcome);
        // End TestCase 2: Login

        //Begin TestCase 3 :Product Samsung galaxy s6
        //Choose product
        WebElement cProduct = edriver.findElement(By.xpath("//a[contains(text(),'Samsung galaxy s6')]"));
        ewait.until((ExpectedConditions.visibilityOf(cProduct)));
        ewait.until(ExpectedConditions.elementToBeClickable(cProduct));
        cProduct.click();
        Thread.sleep(2000);

        //Get text three elements
        //1.Samsung galaxy s6
        waitUntilElementVisible(getTSg6);
        getTextElement(getTSg6);
        //2.$360 *includes tax
        waitUntilElementVisible(getTNum);
        getTextElement(getTNum);
        //3.Line Text
        waitUntilElementVisible(lineText);
        getTextElement(lineText);
        Thread.sleep(2000);
        //Click btn (a) add to cart
        waitUntilElementVisible(btnAddToCart);
        waitUntilElementTobeClickable(btnAddToCart);
        Thread.sleep(4000);
        clickElement(btnAddToCart);
        Thread.sleep(2000);
        try {
            Alert alert = edriver.switchTo().alert();
            String alertText = alert.getText();
            System.out.println("Thông báo hiển thị: " + alertText);
            Thread.sleep(1000);
            alert.accept();
        } catch (NoAlertPresentException e) {
            System.out.println("Không tìm thấy thông báo");
        }
        Thread.sleep(2000);
        //Click linkText Cart
        waitUntilElementVisible(linkCart);
        waitUntilElementTobeClickable(linkCart);
        clickElement(linkCart);
        //Push information of product
        //Pic
        waitUntilElementVisible(pushPicProduct);
        waitUntilElementTobeClickable(pushPicProduct);
        getTextElement(pushPicProduct);
        //Title
        waitUntilElementVisible(pushTitleProduct);
        waitUntilElementTobeClickable(pushTitleProduct);
        Thread.sleep(1000);
        getTextElement(pushTitleProduct);
        //Title Samsung galaxy s6
        Thread.sleep(2000);
        waitUntilElementVisible(pushTitleProductSG6);
        waitUntilElementTobeClickable(pushTitleProductSG6);
        Thread.sleep(1000);
        getTextElement(pushTitleProductSG6);
        // Price
        waitUntilElementVisible(pushPriceProduct);
        waitUntilElementTobeClickable(pushPriceProduct);
        Thread.sleep(1000);
        getTextElement(pushPriceProduct);
        //Price 360
        Thread.sleep(2000);
        waitUntilElementVisible(pushPriceProduct360);
        waitUntilElementTobeClickable(pushPriceProduct360);
        Thread.sleep(1000);
        getTextElement(pushPriceProduct360);
        //Icon x
        waitUntilElementVisible(pushIconProduct);
        waitUntilElementTobeClickable(pushIconProduct);
        Thread.sleep(1000);
        getTextElement(pushIconProduct);
        //Icon x Delete
        waitUntilElementVisible(pushIconProductDelete);
        waitUntilElementTobeClickable(pushIconProductDelete);
        Thread.sleep(1000);
        getTextElement(pushIconProductDelete);
        //Click a delete
        Thread.sleep(4000);
        waitUntilElementVisible(deleteProduct);
        waitUntilElementTobeClickable(deleteProduct);
        clickElement(deleteProduct);
        //End TestCase 3:
        //Begin TestCase 4: Product Nokia Lumia 1520
        //Click home
        Thread.sleep(2000);
        waitUntilElementVisible(combackHome);
        waitUntilElementTobeClickable(combackHome);
        clickElement(combackHome);
        //Chose product Nokia Lumia 1520
        Thread.sleep(2000);
        waitUntilElementVisible(productNokia);
        waitUntilElementTobeClickable(productNokia);
        clickElement(productNokia);
        //Add product to cart
        Thread.sleep(2000);
        waitUntilElementVisible(addproductNokia);
        waitUntilElementTobeClickable(addproductNokia);
        clickElement(addproductNokia);
        Thread.sleep(2000);
        try {
            Alert alert = edriver.switchTo().alert();
            String alertText = alert.getText();
            System.out.println("Thông báo hiển thị: " + alertText);
            Thread.sleep(1000);
            alert.accept();
        } catch (NoAlertPresentException e) {
            System.out.println("Không tìm thấy thông báo");
        }
        //Choose linktext Cart
        Thread.sleep(2000);
        waitUntilElementVisible(chooseCartNokia);
        clickElement(chooseCartNokia);
        //Begin Place Order
        Thread.sleep(1000);
        waitUntilElementVisible(placeOrder);
        waitUntilElementTobeClickable(placeOrder);
        clickElement(placeOrder);
        //Place Order input information
        //1.Name
        Thread.sleep(1000);
        waitUntilElementVisible(inputNamePO);
        waitUntilElementTobeClickable(inputNamePO);
        sendKeyElement(inputNamePO, datafield.getData(0,2));
        //2.Country
        Thread.sleep(1000);
        waitUntilElementVisible(inputCountryPO);
        waitUntilElementTobeClickable(inputCountryPO);
        sendKeyElement(inputCountryPO, datafield.getData(0,3));
        //3.City
        Thread.sleep(1000);
        waitUntilElementVisible(inputCityPO);
        waitUntilElementTobeClickable(inputCityPO);
        sendKeyElement(inputCityPO, datafield.getData(0,4));
        //4.Credit card
        Thread.sleep(1000);
        waitUntilElementVisible(inputCrePO);
        waitUntilElementTobeClickable(inputCrePO);
        sendKeyElement(inputCrePO, datafield.getData(0,5));
        //5.Month
        Thread.sleep(1000);
        waitUntilElementVisible(inputMonthPO);
        waitUntilElementTobeClickable(inputMonthPO);
        sendKeyElement(inputMonthPO, datafield.getData(0,6));
        //6.Year
        Thread.sleep(1000);
        waitUntilElementVisible(inputYearPO);
        waitUntilElementTobeClickable(inputYearPO);
        sendKeyElement(inputYearPO, datafield.getData(0,7));
        //End Place Order

        //Begin Close
        Thread.sleep(1000);
        waitUntilElementVisible(clickClose);
        waitUntilElementTobeClickable(clickClose);
        clickElement(clickClose);
        //End Close
        //End Product Nokia Lumia 1520

        //Begin Logout
        Thread.sleep(1000);
        waitUntilElementVisible(clickLogout);
        waitUntilElementTobeClickable(clickLogout);
        Thread.sleep(4000);
        clickElement(clickLogout);
        Thread.sleep(4000);
        closeWeb();
    }



    @After
    public void tearDown() {
    }
}
