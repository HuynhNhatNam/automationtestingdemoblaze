package TH_AT;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class TA2 {
    public static void main (String[] args) throws InterruptedException {
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.demoblaze.com/index.html");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));

        //Begin Sign up
        WebElement sign = driver.findElement(By.linkText("Sign up"));
        sign.click();
        //User name
        WebElement username = driver.findElement((By.id("sign-username")));
        wait.until((ExpectedConditions.visibilityOf(username)));
        wait.until(ExpectedConditions.elementToBeClickable(username));
        username.sendKeys("NhatNam");
        //Password
        WebElement password = driver.findElement((By.id("sign-password")));
        wait.until((ExpectedConditions.visibilityOf(password)));
        wait.until(ExpectedConditions.elementToBeClickable(password));
        password.sendKeys("123");
        //Click btn
        WebElement btnSign = driver.findElement(By.xpath("//button[contains(text(),'Sign up')]"));
        wait.until((ExpectedConditions.visibilityOf(btnSign)));
        wait.until(ExpectedConditions.elementToBeClickable(btnSign));
        btnSign.click();
        Thread.sleep(2000);
        driver.switchTo().alert().accept();
        //Click close
        WebElement btnClose = driver.findElement(By.xpath("//body/div[@id='signInModal']/div[1]/div[1]/div[3]/button[1]"));
        wait.until((ExpectedConditions.visibilityOf(btnClose)));
        wait.until(ExpectedConditions.elementToBeClickable(btnClose));
        btnClose.click();
        Thread.sleep(2000);
        //End Sign up

        //Begin Login
        //Click login
        WebElement login = driver.findElement(By.linkText("Log in"));
        login.click();
        //Login username
        WebElement LogUsername = driver.findElement(By.id("loginusername"));
        wait.until((ExpectedConditions.visibilityOf(LogUsername)));
        wait.until(ExpectedConditions.elementToBeClickable(LogUsername));
        LogUsername.sendKeys("NhatNam");
        //Login password
        WebElement LogPassword = driver.findElement(By.id("loginpassword"));
        wait.until((ExpectedConditions.visibilityOf(LogPassword)));
        wait.until(ExpectedConditions.elementToBeClickable(LogPassword));
        LogPassword.sendKeys("123");
        //Click Btn Login
        WebElement BtnLog = driver.findElement((By.xpath("//button[contains(text(),'Log in')]")));
        wait.until((ExpectedConditions.visibilityOf(BtnLog)));
        wait.until(ExpectedConditions.elementToBeClickable(BtnLog));
        BtnLog.click();
        Thread.sleep(2000);
        //End Login

        //Begin Product Samsung galaxy s6
        //Choose product
        WebElement cProduct = driver.findElement(By.xpath("//a[contains(text(),'Samsung galaxy s6')]"));
        wait.until((ExpectedConditions.visibilityOf(cProduct)));
        wait.until(ExpectedConditions.elementToBeClickable(cProduct));
        cProduct.click();
        Thread.sleep(2000);
        //Get text three elements
        //1.Samsung galaxy s6
        WebElement getTSg6 = driver.findElement(By.xpath("//h2[contains(text(),'Samsung galaxy s6')]"));
        System.out.println(getTSg6.getText());
        //2.$360 *includes tax
        WebElement getTNum = driver.findElement(By.xpath("//body/div[5]/div[1]/div[2]/h3[1]"));
        System.out.println(getTNum.getText());
        //3.Line Text
        WebElement lineText = driver.findElement(By.id("more-information"));
        System.out.println(lineText.getText());
        Thread.sleep(2000);
        //Click btn (a) add to cart
        WebElement btnAddToCart = driver.findElement(By.xpath("//a[contains(text(),'Add to cart')]"));
        wait.until((ExpectedConditions.visibilityOf(btnAddToCart)));
        wait.until(ExpectedConditions.elementToBeClickable(btnAddToCart));
        btnAddToCart.click();
        Thread.sleep(2000);
        driver.switchTo().alert().accept();
        Thread.sleep(2000);
        //Click linkText Cart
        WebElement linkCart = driver.findElement(By.linkText("Cart"));
        wait.until((ExpectedConditions.visibilityOf(linkCart)));
        wait.until(ExpectedConditions.elementToBeClickable(linkCart));
        linkCart.click();
        //Push information of product
        //Pic
        WebElement pushPicProduct = driver.findElement(By.xpath("//thead/tr/th[1]"));
        wait.until((ExpectedConditions.visibilityOf(pushPicProduct)));
        wait.until(ExpectedConditions.elementToBeClickable(pushPicProduct));
        Thread.sleep(1000);
        System.out.println(pushPicProduct.getText());
        //Title
        WebElement pushTitleProduct = driver.findElement(By.xpath("//thead/tr/th[2]"));
        wait.until((ExpectedConditions.visibilityOf(pushTitleProduct)));
        wait.until(ExpectedConditions.elementToBeClickable(pushTitleProduct));
        Thread.sleep(1000);
        System.out.println(pushTitleProduct.getText());
        //Title Samsung galaxy s6
        Thread.sleep(2000);
        WebElement pushTitleProductSG6 = driver.findElement(By.xpath("//td[contains(text(),'Samsung galaxy s6')]"));
        wait.until((ExpectedConditions.visibilityOf(pushTitleProductSG6)));
        wait.until(ExpectedConditions.elementToBeClickable(pushTitleProductSG6));
        Thread.sleep(1000);
        System.out.println(pushTitleProductSG6.getText());
        // Price
        WebElement pushPriceProduct = driver.findElement(By.xpath("//thead/tr/th[3]"));
        wait.until((ExpectedConditions.visibilityOf(pushPriceProduct)));
        wait.until(ExpectedConditions.elementToBeClickable(pushPriceProduct));
        Thread.sleep(1000);
        System.out.println(pushPriceProduct.getText());
        //Price 360
        Thread.sleep(2000);
        WebElement pushPriceProduct360 = driver.findElement(By.xpath("//td[contains(text(),'360')]"));
        wait.until((ExpectedConditions.visibilityOf(pushPriceProduct360)));
        wait.until(ExpectedConditions.elementToBeClickable(pushPriceProduct360));
        Thread.sleep(1000);
        System.out.println(pushPriceProduct360.getText());
        //Icon x
        WebElement pushIconProduct = driver.findElement(By.xpath("//thead/tr/th[4]"));
        wait.until((ExpectedConditions.visibilityOf(pushIconProduct)));
        wait.until(ExpectedConditions.elementToBeClickable(pushIconProduct));
        Thread.sleep(1000);
        System.out.println(pushIconProduct.getText());
        //Icon x Delete
        WebElement pushIconProductDelete = driver.findElement(By.xpath("//body[1]/div[6]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[4]"));
        wait.until((ExpectedConditions.visibilityOf(pushIconProductDelete)));
        wait.until(ExpectedConditions.elementToBeClickable(pushIconProductDelete));
        Thread.sleep(1000);
        System.out.println(pushIconProductDelete.getText());
        //Click a delete
        Thread.sleep(4000);
        WebElement deleteProduct = driver.findElement(By.linkText("Delete"));
        wait.until((ExpectedConditions.visibilityOf(deleteProduct)));
        wait.until(ExpectedConditions.elementToBeClickable(deleteProduct));
        deleteProduct.click();
        //End Product Samsung galaxy s6

        //Begin Product Nokia Lumia 1520
        //Click home
        Thread.sleep(2000);
        WebElement combackHome = driver.findElement(By.xpath("//body/nav[1]/div[1]/div[1]/ul[1]/li[1]/a[1]"));
        wait.until((ExpectedConditions.visibilityOf(combackHome)));
        wait.until(ExpectedConditions.elementToBeClickable(combackHome));
        combackHome.click();
        //Chose product Nokia Lumia 1520
        Thread.sleep(2000);
        WebElement productNokia = driver.findElement(By.xpath("//a[contains(text(),'Nokia lumia 1520')]"));
        wait.until((ExpectedConditions.visibilityOf(productNokia)));
        wait.until(ExpectedConditions.elementToBeClickable(productNokia));
        productNokia.click();
        //Add product to cart
        Thread.sleep(2000);
        WebElement addproductNokia = driver.findElement(By.xpath("//a[contains(text(),'Add to cart')]"));
        wait.until((ExpectedConditions.visibilityOf(addproductNokia)));
        wait.until(ExpectedConditions.elementToBeClickable(addproductNokia));
        addproductNokia.click();
        Thread.sleep(2000);
        driver.switchTo().alert().accept();
        //Choose linktext Cart
        Thread.sleep(2000);
        WebElement chooseCartNokia = driver.findElement(By.linkText("Cart"));
        chooseCartNokia.click();

        //Begin Place Order
        Thread.sleep(2000);
        WebElement placeOrder = driver.findElement(By.xpath("//button[contains(text(),'Place Order')]"));
        wait.until((ExpectedConditions.visibilityOf(placeOrder)));
        wait.until(ExpectedConditions.elementToBeClickable(placeOrder));
        placeOrder.click();
        //Place Order input information
        //1.Name
        Thread.sleep(1000);
        WebElement inputNamePO = driver.findElement(By.id("name"));
        wait.until((ExpectedConditions.visibilityOf(inputNamePO)));
        wait.until(ExpectedConditions.elementToBeClickable(inputNamePO));
        inputNamePO.sendKeys("Huỳnh Nhật Nam");
        //2.Country
        Thread.sleep(1000);
        WebElement inputCountryPO = driver.findElement(By.id("country"));
        wait.until((ExpectedConditions.visibilityOf(inputCountryPO)));
        wait.until(ExpectedConditions.elementToBeClickable(inputCountryPO));
        inputCountryPO.sendKeys("Việt Nam");
        //3.City
        Thread.sleep(1000);
        WebElement inputCityPO = driver.findElement(By.id("city"));
        wait.until((ExpectedConditions.visibilityOf(inputCityPO)));
        wait.until(ExpectedConditions.elementToBeClickable(inputCityPO));
        inputCityPO.sendKeys("Thành phố Hồ Chí Minh");
        //4.Credit card
        Thread.sleep(1000);
        WebElement inputCrePO = driver.findElement(By.id("card"));
        wait.until((ExpectedConditions.visibilityOf(inputCrePO)));
        wait.until(ExpectedConditions.elementToBeClickable(inputCrePO));
        inputCrePO.sendKeys("1234 5678 9012 3456");
        //5.Month
        Thread.sleep(1000);
        WebElement inputMonthPO = driver.findElement(By.id("month"));
        wait.until((ExpectedConditions.visibilityOf(inputMonthPO)));
        wait.until(ExpectedConditions.elementToBeClickable(inputMonthPO));
        inputMonthPO.sendKeys("June");
        //6.Year
        Thread.sleep(1000);
        WebElement inputYearPO = driver.findElement(By.id("year"));
        wait.until((ExpectedConditions.visibilityOf(inputYearPO)));
        wait.until(ExpectedConditions.elementToBeClickable(inputYearPO));
        inputYearPO.sendKeys("2023");
        //End Place Order

        //Begin Close
        Thread.sleep(1000);
        WebElement clickClose = driver.findElement(By.xpath("//body/div[@id='orderModal']/div[1]/div[1]/div[3]/button[1]"));
        wait.until((ExpectedConditions.visibilityOf(clickClose)));
        wait.until(ExpectedConditions.elementToBeClickable(clickClose));
        clickClose.click();
        //End Close
        //End Product Nokia Lumia 1520

        //Begin Logout
        Thread.sleep(1000);
        WebElement clickLogout = driver.findElement(By.id("logout2"));
        wait.until((ExpectedConditions.visibilityOf(clickLogout)));
        wait.until(ExpectedConditions.elementToBeClickable(clickLogout));
        clickLogout.click();
        driver.close();
    }
}
