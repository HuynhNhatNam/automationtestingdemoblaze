package FinalProject_DemoBlaze;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CommonPageObjectFinal {
    public static WebDriver edriver;
    public static   WebDriverWait ewait;

    @FindBy(how = How.XPATH, using = "//a[@id='signin2']")
    public WebElement Signin1; //Sign up

    @FindBy(how = How.XPATH, using = "//input[@id='sign-username']")
    public WebElement SignUserName; //Sign in username

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Sign up')]")
    public WebElement SignUserBtnClick; //Click btn Sign in

    @FindBy(how = How.XPATH, using = "//div[@id='signInModal']//div[@class='modal-footer']//child::button[contains(text(),'Close')]")
    public WebElement SignCloserBtnClick; //Click btn Close
    @FindBy(how = How.XPATH, using = "//input[@id='sign-password']")
    public WebElement SignUserPass;
    @FindBy(how = How.XPATH, using = "//*[@id='login2']")
    public WebElement Login1;

    @FindBy(how = How.XPATH, using = "//*[@id='loginusername']")
    public  WebElement userName;

    @FindBy(how = How.XPATH, using = "//*[@id='loginpassword']")
    public  WebElement userPass;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Log in')]")
    public  WebElement Loguser;

//    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Samsung galaxy s6')]")
//    public  WebElement cProduct;

    @FindBy(how =How.XPATH, using = "//h2[contains(text(),'Samsung galaxy s6')]")
    public WebElement getTSg6;
    @FindBy(how =How.XPATH, using = "//h3[contains(text(),'$360')]")
    public WebElement getTNum;

    @FindBy(how = How.ID, using = "more-information")
    public WebElement lineText;

    @FindBy(how =How.XPATH, using = "//div[@class='row']//div//following::a[contains(text(),'Add to cart')]")
    public WebElement btnAddToCart;

    @FindBy(how =How.LINK_TEXT, using = "Cart")
    public WebElement linkCart;

    @FindBy(how =How.XPATH, using = "//div[@id='page-wrapper']//thead/tr/th[contains(text(),'Pic')]")
    public WebElement pushPicProduct;

    @FindBy(how =How.XPATH, using = "//div[@id='page-wrapper']//thead/tr/th[contains(text(),'Title')]")
    public WebElement pushTitleProduct;

    @FindBy(how =How.XPATH, using = "//td[contains(text(),'Samsung galaxy s6')]")
    public WebElement pushTitleProductSG6;

    @FindBy(how =How.XPATH, using = "//div[@id='page-wrapper']//thead/tr/th[contains(text(),'Price')]")
    public WebElement pushPriceProduct;

    @FindBy(how =How.XPATH, using = "//td[contains(text(),'360')]")
    public WebElement pushPriceProduct360;

    @FindBy(how =How.XPATH, using = "//div[@id='page-wrapper']//thead/tr/th[contains(text(),'x')]")
    public WebElement pushIconProduct;

    @FindBy(how =How.XPATH, using = "//body[1]/div[6]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[4]")
    public WebElement pushIconProductDelete;

    @FindBy(how =How.XPATH, using = "//tbody[@id='tbodyid']/tr[@class='success']//following::a")
    public WebElement deleteProduct;

    @FindBy(how =How.XPATH, using = "//li[@class='nav-item active']/a[contains(text(),'Home')]")
    public WebElement combackHome;

    @FindBy(how =How.XPATH, using = "//a[contains(text(),'Nokia lumia 1520')]")
    public WebElement productNokia;

    @FindBy(how =How.XPATH, using = "//a[contains(text(),'Add to cart')]")
    public WebElement addproductNokia;

    @FindBy(how =How.LINK_TEXT, using = "Cart")
    public WebElement chooseCartNokia;

    @FindBy(how =How.XPATH, using = "//button[contains(text(),'Place Order')]")
    public WebElement placeOrder;

    @FindBy(how =How.ID, using = "name")
    public WebElement inputNamePO;

    @FindBy(how =How.ID, using = "country")
    public WebElement inputCountryPO;

    @FindBy(how =How.ID, using = "city")
    public WebElement inputCityPO;

    @FindBy(how =How.ID, using = "card")
    public WebElement inputCrePO;

    @FindBy(how =How.ID, using = "month")
    public WebElement inputMonthPO;

    @FindBy(how =How.ID, using = "year")
    public WebElement inputYearPO;
    @FindBy(how =How.XPATH, using = "//a[@id='nameofuser' and contains(text(),'Welcome')]")
    public WebElement welcome;

    @FindBy(how =How.XPATH, using = "//body/div[@id='orderModal']/div[1]/div[1]/div[3]/button[1]")
    public WebElement clickClose;

    @FindBy(how =How.ID, using = "logout2")
    public WebElement clickLogout;

    public CommonPageObjectFinal(WebDriver driver) throws Exception {
        edriver = new ChromeDriver();
//        ChromeOptions opt = new ChromeOptions();
//        opt.setHeadless(true);
//        edriver = new ChromeDriver(opt);
        PageFactory.initElements(edriver, this);
        ewait = new WebDriverWait(edriver, Duration.ofSeconds(30L));
        edriver.manage().window().maximize();
        edriver.get("https://www.demoblaze.com/index.html");
    }

    public void getTextElement(WebElement element) {
        System.out.println(element.getText());
    }

    public void clickElement(WebElement element) {
        element.click();
    }

    public void sendKeyElement(WebElement element, String value) {
        element.sendKeys(value);
    }
    public void closeWeb() throws Exception {
        edriver.close();
    }
    public void waitUntilElementVisible(WebElement element)  {
        int tryTimes = 0;
        while (tryTimes < 2) {
            try {
                ewait.until(ExpectedConditions.visibilityOf(element));
                break;
            }
            catch (StaleElementReferenceException se) {
                tryTimes ++;
                if (tryTimes == 2)
                    throw se;
            }
        }
    }

    public void waitUntilElementVisible(String path) {
        int tryTimes = 0;
        while (tryTimes < 2) {
            try {
                WebElement element = edriver.findElement(By.id(path));
                ewait.until(ExpectedConditions.visibilityOf(element));
                break;
            }
            catch (StaleElementReferenceException se) {
                tryTimes ++;
                if (tryTimes == 2)
                    throw se;
            }
        }
    }
    public void waitUntilElementTobeClickable(WebElement element) {
        int tryTimes = 0;
        while (tryTimes < 2) {
            try {
                ewait.until(ExpectedConditions.elementToBeClickable(element));
                break;
            }
            catch (StaleElementReferenceException se) {
                tryTimes ++;
                if (tryTimes == 2)
                    throw se;
            }
        }
    }
}
