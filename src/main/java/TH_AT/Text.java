package TH_AT;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Text  {

    public static void main(String[] args) throws InterruptedException {
        ChromeDriver dr = new ChromeDriver();
        WebDriverManager web = WebDriverManager.chromedriver();
        WebDriverWait wait = new WebDriverWait(dr, Duration.ofSeconds(4));
        web.setup();
        dr.manage().window().maximize();
        dr.get("https://magento.softwaretestingboard.com/customer/account/login/referer/aHR0cHM6Ly9tYWdlbnRvLnNvZnR3YXJldGVzdGluZ2JvYXJkLmNvbS8%2C/");

        //Create account
        WebElement createAcount = dr.findElement(By.xpath("//body[1]/div[1]/main[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/a[1]/span[1]"));
        wait.until(ExpectedConditions.visibilityOf(createAcount));
        createAcount.click();
        //First name
        WebElement firstName = dr.findElement(By.id("firstname"));
        wait.until(ExpectedConditions.visibilityOf(firstName));
        firstName.sendKeys("Huỳnh Nhật");
        Thread.sleep(1000);
        //Last name
        WebElement lastName = dr.findElement(By.id("lastname"));
        wait.until(ExpectedConditions.visibilityOf(lastName));
        lastName.sendKeys("Nam");
        Thread.sleep(1000);
        //Email
        WebElement emailUser = dr.findElement(By.id("email_address"));
        wait.until(ExpectedConditions.visibilityOf(emailUser));
        emailUser.sendKeys("nhatnam2026@gmail.com");
        Thread.sleep(1000);
        //Pass
        WebElement passlUser = dr.findElement(By.id("password"));
        wait.until(ExpectedConditions.visibilityOf(passlUser));
        passlUser.sendKeys("HuynhNhatNam@123");
        Thread.sleep(1000);
        //Confirm pass
        WebElement passlConfirmUser = dr.findElement(By.id("password-confirmation"));
        wait.until(ExpectedConditions.visibilityOf(passlConfirmUser));
        passlConfirmUser.sendKeys("HuynhNhatNam@123");
        Thread.sleep(1000);
        //Click btn
        WebElement createSignup = dr.findElement(By.xpath("//body[1]/div[1]/main[1]/div[3]/div[1]/form[1]/div[1]/div[1]/button[1]/span[1]"));
        wait.until(ExpectedConditions.visibilityOf(createSignup));
        createSignup.click();
        Thread.sleep(1000);
        //click arrow dowm
        WebElement arrowDown = dr.findElement(By.xpath("//header/div[1]/div[1]/ul[1]/li[2]/span[1]/button[1]"));
        wait.until(ExpectedConditions.visibilityOf(arrowDown));
        arrowDown.click();
        Thread.sleep(1000);
        //Click signout
        WebElement signOut = dr.findElement(By.xpath("//header/div[1]/div[1]/ul[1]/li[2]/div[1]/ul[1]/li[3]/a[1]"));
        wait.until(ExpectedConditions.visibilityOf(signOut));
        signOut.click();
        Thread.sleep(1000);
        //Click signin
        WebElement signIn = dr.findElement(By.xpath("//header/div[1]/div[1]/ul[1]/li[2]/a[1]"));
        wait.until(ExpectedConditions.visibilityOf(signIn));
        signIn.click();
        Thread.sleep(1000);
        //Input email
        WebElement logEmail = dr.findElement(By.id("email"));
        wait.until(ExpectedConditions.visibilityOf(logEmail));
        logEmail.sendKeys("nhatnam2026@gmail.com");
        Thread.sleep(1000);
        //Input pass login
        WebElement passLogin = dr.findElement(By.id("pass"));
        wait.until(ExpectedConditions.visibilityOf(passLogin));
        passLogin.sendKeys("HuynhNhatNam@123");
        Thread.sleep(1000);





    }

}
